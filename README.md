### This project is a proof of concept carried out with the purpose of solidifying some concepts, among them [Spring Web](https://spring.io/web-applications), [JPA](https://spring.io/projects/spring-data-jpa) and some business rule concepts in terms of systems for asset security

#### Author: [Alvaro Pereira](https://br.linkedin.com/in/alvarocfpereira)