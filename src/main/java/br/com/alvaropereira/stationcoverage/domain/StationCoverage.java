package br.com.alvaropereira.stationcoverage.domain;

import jakarta.persistence.*;

@Entity
@Table(name = "station_coverage")
public class StationCoverage {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "station_id", referencedColumnName = "id")
    private Station station;

    @ManyToOne
    @JoinColumn(name = "coverage_id", referencedColumnName = "id")
    private Coverage coverage;

    public StationCoverage() {}

    public StationCoverage(String id, Station station, Coverage coverage) {
        this.id = id;
        this.station = station;
        this.coverage = coverage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Coverage getCoverage() {
        return coverage;
    }

    public void setCoverage(Coverage coverage) {
        this.coverage = coverage;
    }
}
