package br.com.alvaropereira.stationcoverage.domain;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<CustomerTagAssociation> customerTagAssociationList;

    public Customer() {}

    public Customer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CustomerTagAssociation> getCustomerTagAssociationList() {
        return customerTagAssociationList;
    }

    public void setCustomerTagAssociationList(List<CustomerTagAssociation> customerTagAssociationList) {
        this.customerTagAssociationList = customerTagAssociationList;
    }
}
