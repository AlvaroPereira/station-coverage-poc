package br.com.alvaropereira.stationcoverage.domain;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "coverage_types")
public class CoverageType {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "coverageType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Station> stationList;

    public CoverageType() {}

    public CoverageType(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Station> getStationList() {
        return stationList;
    }

    public void setStationList(List<Station> stationList) {
        this.stationList = stationList;
    }
}
