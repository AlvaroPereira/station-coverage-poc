package br.com.alvaropereira.stationcoverage.domain;

import jakarta.persistence.*;

@Entity
@Table(name = "station_attendance_type")
public class StationAttendanceType {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "station_id", referencedColumnName = "id")
    private Station station;

    @ManyToOne
    @JoinColumn(name = "attendance_type_id", referencedColumnName = "id")
    private AttendanceType attendanceType;

    public StationAttendanceType() {}

    public StationAttendanceType(String id, Station station, AttendanceType attendanceType) {
        this.id = id;
        this.station = station;
        this.attendanceType = attendanceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public AttendanceType getAttendanceType() {
        return attendanceType;
    }

    public void setAttendanceType(AttendanceType attendanceType) {
        this.attendanceType = attendanceType;
    }
}
