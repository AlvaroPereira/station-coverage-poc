package br.com.alvaropereira.stationcoverage.domain;


import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "attendance_type")
public class AttendanceType {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "attendanceType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<StationAttendanceType> stationAttendanceTypeList;

    public AttendanceType() {}

    public AttendanceType(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StationAttendanceType> getStationAttendanceTypeList() {
        return stationAttendanceTypeList;
    }

    public void setStationAttendanceTypeList(List<StationAttendanceType> stationAttendanceTypeList) {
        this.stationAttendanceTypeList = stationAttendanceTypeList;
    }
}
