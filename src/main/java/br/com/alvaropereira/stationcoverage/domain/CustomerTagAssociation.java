package br.com.alvaropereira.stationcoverage.domain;
import jakarta.persistence.*;

@Entity
@Table(name = "customer_tag_association")
public class CustomerTagAssociation {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "tag_id", referencedColumnName = "id")
    private CustomerTag customerTag;

    public CustomerTagAssociation() {}

    public CustomerTagAssociation(String id, Customer customer, CustomerTag customerTag) {
        this.id = id;
        this.customer = customer;
        this.customerTag = customerTag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CustomerTag getCustomerTag() {
        return customerTag;
    }

    public void setCustomerTag(CustomerTag customerTag) {
        this.customerTag = customerTag;
    }
}
