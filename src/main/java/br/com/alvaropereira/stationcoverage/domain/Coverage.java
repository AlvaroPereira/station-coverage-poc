package br.com.alvaropereira.stationcoverage.domain;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "coverage")
public class Coverage {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "coverage", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<StationCoverage> stationCoverageList;

    public Coverage() {}

    public Coverage(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StationCoverage> getStationCoverageList() {
        return stationCoverageList;
    }

    public void setStationCoverageList(List<StationCoverage> stationCoverageList) {
        this.stationCoverageList = stationCoverageList;
    }
}
