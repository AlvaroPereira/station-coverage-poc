package br.com.alvaropereira.stationcoverage.domain;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "station")
public class Station {
    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "station", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<StationCoverage> stationCoverageList;

    @OneToMany(mappedBy = "station", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<StationAttendanceType> stationAttendanceTypeList;

    @ManyToOne
    @JoinColumn(name = "coverage_type_id", nullable = false)
    private CoverageType coverageType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StationCoverage> getStationCoverageList() {
        return stationCoverageList;
    }

    public void setStationCoverageList(List<StationCoverage> stationCoverageList) {
        this.stationCoverageList = stationCoverageList;
    }

    public List<StationAttendanceType> getStationAttendanceTypeList() {
        return stationAttendanceTypeList;
    }

    public void setStationAttendanceTypeList(List<StationAttendanceType> stationAttendanceTypeList) {
        this.stationAttendanceTypeList = stationAttendanceTypeList;
    }

    public CoverageType getCoverageType() {
        return coverageType;
    }

    public void setCoverageType(CoverageType coverageType) {
        this.coverageType = coverageType;
    }
}

