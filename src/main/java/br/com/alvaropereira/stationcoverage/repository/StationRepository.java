package br.com.alvaropereira.stationcoverage.repository;

import br.com.alvaropereira.stationcoverage.domain.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRepository extends JpaRepository<Station, String> {

}