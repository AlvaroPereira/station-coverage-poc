package br.com.alvaropereira.stationcoverage.repository;

import br.com.alvaropereira.stationcoverage.domain.CoverageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoverageTypeRepository extends JpaRepository<CoverageType, String> {
}
