package br.com.alvaropereira.stationcoverage.controller;

import br.com.alvaropereira.stationcoverage.util.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class PingController {

    @GetMapping("/ping")
    public ResponseEntity pong() {

        var hashMap = new HashMap<String, Object>();
        hashMap.put("pong", true);

        return Response.build(true, "Success", hashMap, HttpStatus.OK);
    }

}
