package br.com.alvaropereira.stationcoverage.controller;

import br.com.alvaropereira.stationcoverage.repository.ICoverageTypeRepository;
import br.com.alvaropereira.stationcoverage.util.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoverageTypeController {
    private final ICoverageTypeRepository coverageTypeRepository;

    public CoverageTypeController(ICoverageTypeRepository coverageTypeRepository) {
        this.coverageTypeRepository = coverageTypeRepository;
    }

    @GetMapping("/coverage-types")
    public ResponseEntity all() {
        var coverageTypes = this.coverageTypeRepository.findAll();
        return Response.build(true, "Success", coverageTypes, HttpStatus.OK);
    }
}
