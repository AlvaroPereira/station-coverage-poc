package br.com.alvaropereira.stationcoverage.controller;

import br.com.alvaropereira.stationcoverage.domain.Station;
import br.com.alvaropereira.stationcoverage.dto.CreateStationDto;
import br.com.alvaropereira.stationcoverage.exception.BadRequestException;
import br.com.alvaropereira.stationcoverage.repository.CoverageTypeRepository;
import br.com.alvaropereira.stationcoverage.repository.StationRepository;
import br.com.alvaropereira.stationcoverage.util.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class StationController {

    private CoverageTypeRepository coverageTypeRepository;
    private StationRepository stationRepository;

    public StationController(CoverageTypeRepository coverageTypeRepository, StationRepository stationRepository) {
        this.coverageTypeRepository = coverageTypeRepository;
        this.stationRepository = stationRepository;
    }

    @PostMapping("/stations")
    public ResponseEntity create(@RequestBody CreateStationDto createStationDto) {

        try {
            var coverageType = coverageTypeRepository.findById(createStationDto.coverageTypeId);
            if(coverageType.isEmpty()) throw new BadRequestException("coverageTypeId not found");

            var station = new Station();
            station.setId(UUID.randomUUID().toString());
            station.setName(createStationDto.name);
            station.setCoverageType(coverageType.get());

            this.stationRepository.save(station);

        } catch (BadRequestException badRequestException) {
            return Response.build(false, badRequestException.getMessage(), null, HttpStatus.BAD_REQUEST);
        }

        return Response.build(true, "Created", null, HttpStatus.CREATED);
    }
}
