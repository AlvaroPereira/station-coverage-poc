package br.com.alvaropereira.stationcoverage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StationCoverageApplication {

    public static void main(String[] args) {
        SpringApplication.run(StationCoverageApplication.class, args);
    }

}
