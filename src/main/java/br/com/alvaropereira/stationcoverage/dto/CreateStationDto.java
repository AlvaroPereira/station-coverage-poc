package br.com.alvaropereira.stationcoverage.dto;

public class CreateStationDto {
    public String name;
    public String coverageTypeId;
}
